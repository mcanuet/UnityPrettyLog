using UnityEditor;
using System.Collections.Generic;

namespace UnityPrettyLog
{
	[InitializeOnLoad]
	public static class PrettyLogMenus
	{
		#region Instances
		private const string menuName = "Unity Pretty Log";

		private const string levelSettingsMenu = menuName + "/Log Level Setting/";
		private const string levelSettingsMenuDebug = levelSettingsMenu + "Debug";
		private const string levelSettingsMenuInfo = levelSettingsMenu + "Info";
		private const string levelSettingsMenuTest = levelSettingsMenu + "Test";
		private const string levelSettingsMenuWarning = levelSettingsMenu + "Warning";
		private const string levelSettingsMenuError = levelSettingsMenu + "Error";
		private const string levelSettingsMenuCritical = levelSettingsMenu + "Critical";

		private const string categoriesSettingsMenu = menuName + "/Log Categories Setting/";
		private const string categorySettingsMenuScript = categoriesSettingsMenu + "Script";
		private const string categorySettingsMenuScript_Init = categoriesSettingsMenu + "Script_Init";
		private const string categorySettingsMenuScript_Data = categoriesSettingsMenu + "Script_Data";
		private const string categorySettingsMenuScript_Utils = categoriesSettingsMenu + "Script_Utils";
		private const string categorySettingsMenuScript_Engine = categoriesSettingsMenu + "Script_Engine";
		private const string categorySettingsMenuScript_Editor = categoriesSettingsMenu + "Script_Editor";
		private const string categorySettingsMenuUI = categoriesSettingsMenu + "UI";
		private const string categorySettingsMenuNetwork = categoriesSettingsMenu + "Network";
		private const string categorySettingsMenuGame = categoriesSettingsMenu + "Game";
		private const string categorySettingsMenuGame_Loop = categoriesSettingsMenu + "Game_Loop";
		private const string categorySettingsMenuGame_Save = categoriesSettingsMenu + "Game_Save";
		private const string categorySettingsMenuGame_Load = categoriesSettingsMenu + "Game_Load";
		private const string categorySettingsMenuData = categoriesSettingsMenu + "Data";
		private const string categorySettingsMenuData_Save = categoriesSettingsMenu + "Data_Save";
		private const string categorySettingsMenuData_Load = categoriesSettingsMenu + "Data_Load";
		#endregion

		#region Init
		static PrettyLogMenus()
		{
			UpdateCheckStatusLogLevel(PrettyLog.prettyLogSettings.levelToLog);

			Menu.SetChecked(categorySettingsMenuScript, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script));
			Menu.SetChecked(categorySettingsMenuScript_Init, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script_Init));
			Menu.SetChecked(categorySettingsMenuScript_Data, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script_Data));
			Menu.SetChecked(categorySettingsMenuScript_Utils, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script_Utils));
			Menu.SetChecked(categorySettingsMenuScript_Engine, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script_Engine));
			Menu.SetChecked(categorySettingsMenuScript_Editor, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Script_Editor));
			Menu.SetChecked(categorySettingsMenuUI, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.UI));
			Menu.SetChecked(categorySettingsMenuNetwork, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Network));
			Menu.SetChecked(categorySettingsMenuGame, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Game));
			Menu.SetChecked(categorySettingsMenuGame_Loop, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Game_Loop));
			Menu.SetChecked(categorySettingsMenuGame_Save, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Game_Save));
			Menu.SetChecked(categorySettingsMenuGame_Load, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Game_Load));
			Menu.SetChecked(categorySettingsMenuData, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Data));
			Menu.SetChecked(categorySettingsMenuData_Save, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Data_Save));
			Menu.SetChecked(categorySettingsMenuData_Load, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(PrettyLog.LogCategory.Data_Load));

			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Init, "Unity pretty log menu Initialized");
		}
		#endregion

		#region Menus
		[MenuItem(menuName + "/Focus settings", false, 10)]
		private static void FocusLogSettings()
		{
			Selection.activeObject = PrettyLog.prettyLogSettings;
			EditorGUIUtility.PingObject(PrettyLog.prettyLogSettings);
		}

		[MenuItem(menuName + "/Instantiate PrettyLog game object", false, 11)]
		private static void InstantiateLogUIGO()
		{
			PrefabUtility.InstantiatePrefab(PrettyLog.prettyLogSettings.PrettyLogWindow);
		}

		#region LogLevel
		[MenuItem(levelSettingsMenuDebug, false, 20)]
		private static void ToggleDebugLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Debug);

		[MenuItem(levelSettingsMenuInfo, false, 20)]
		private static void ToggleInfoLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Info);

		[MenuItem(levelSettingsMenuTest, false, 20)]
		private static void ToggleTestLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Test);

		[MenuItem(levelSettingsMenuWarning, false, 20)]
		private static void ToggleWarningLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Warning);

		[MenuItem(levelSettingsMenuError, false, 20)]
		private static void ToggleErrorLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Error);

		[MenuItem(levelSettingsMenuCritical, false, 20)]
		private static void ToggleCriticalLevel() => UpdateLevelToLog(PrettyLog.LogLevel.Critical);
		#endregion

		#region LogCategory
		[MenuItem(categorySettingsMenuScript, false, 30)]
		private static void ToogleLogCategoryScript() => ToogleLogCategory(PrettyLog.LogCategory.Script, categorySettingsMenuScript);

		[MenuItem(categorySettingsMenuScript_Init, false, 30)]
		private static void ToogleLogCategoryScript_Init() => ToogleLogCategory(PrettyLog.LogCategory.Script_Init, categorySettingsMenuScript_Init);

		[MenuItem(categorySettingsMenuScript_Data, false, 30)]
		private static void ToogleLogCategoryScript_Data() => ToogleLogCategory(PrettyLog.LogCategory.Script_Data, categorySettingsMenuScript_Data);

		[MenuItem(categorySettingsMenuScript_Utils, false, 30)]
		private static void ToogleLogCategoryScript_Utils() => ToogleLogCategory(PrettyLog.LogCategory.Script_Utils, categorySettingsMenuScript_Utils);

		[MenuItem(categorySettingsMenuScript_Engine, false, 30)]
		private static void ToogleLogCategoryScript_Engine() => ToogleLogCategory(PrettyLog.LogCategory.Script_Engine, categorySettingsMenuScript_Engine);

		[MenuItem(categorySettingsMenuScript_Editor, false, 30)]
		private static void ToogleLogCategoryScript_Editor() => ToogleLogCategory(PrettyLog.LogCategory.Script_Editor, categorySettingsMenuScript_Editor);

		[MenuItem(categorySettingsMenuUI, false, 30)]
		private static void ToogleLogCategoryUI() => ToogleLogCategory(PrettyLog.LogCategory.UI, categorySettingsMenuUI);

		[MenuItem(categorySettingsMenuNetwork, false, 30)]
		private static void ToogleLogCategoryNetwork() => ToogleLogCategory(PrettyLog.LogCategory.Network, categorySettingsMenuNetwork);

		[MenuItem(categorySettingsMenuGame, false, 30)]
		private static void ToogleLogCategoryGame() => ToogleLogCategory(PrettyLog.LogCategory.Game, categorySettingsMenuGame);

		[MenuItem(categorySettingsMenuGame_Loop, false, 30)]
		private static void ToogleLogCategoryGame_Loop() => ToogleLogCategory(PrettyLog.LogCategory.Game_Loop, categorySettingsMenuGame_Loop);

		[MenuItem(categorySettingsMenuGame_Save, false, 30)]
		private static void ToogleLogCategoryGame_Save() => ToogleLogCategory(PrettyLog.LogCategory.Game_Save, categorySettingsMenuGame_Save);

		[MenuItem(categorySettingsMenuGame_Load, false, 30)]
		private static void ToogleLogCategoryGame_Load() => ToogleLogCategory(PrettyLog.LogCategory.Game_Load, categorySettingsMenuGame_Load);

		[MenuItem(categorySettingsMenuData, false, 30)]
		private static void ToogleLogCategoryData() => ToogleLogCategory(PrettyLog.LogCategory.Data, categorySettingsMenuData);

		[MenuItem(categorySettingsMenuData_Save, false, 30)]
		private static void ToogleLogCategoryData_Save() => ToogleLogCategory(PrettyLog.LogCategory.Data_Save, categorySettingsMenuData_Save);

		[MenuItem(categorySettingsMenuData_Load, false, 30)]
		private static void ToogleLogCategoryData_Load() => ToogleLogCategory(PrettyLog.LogCategory.Data_Load, categorySettingsMenuData_Load);
		#endregion

		#endregion

		#region flow
		private static void UpdateCheckStatusLogLevel(PrettyLog.LogLevel logLevel)
		{
			Menu.SetChecked(levelSettingsMenuDebug, PrettyLog.LogLevel.Debug == PrettyLog.prettyLogSettings.levelToLog);
			Menu.SetChecked(levelSettingsMenuInfo, PrettyLog.LogLevel.Info == PrettyLog.prettyLogSettings.levelToLog);
			Menu.SetChecked(levelSettingsMenuTest, PrettyLog.LogLevel.Test == PrettyLog.prettyLogSettings.levelToLog);
			Menu.SetChecked(levelSettingsMenuWarning, PrettyLog.LogLevel.Warning == PrettyLog.prettyLogSettings.levelToLog);
			Menu.SetChecked(levelSettingsMenuError, PrettyLog.LogLevel.Error == PrettyLog.prettyLogSettings.levelToLog);
			Menu.SetChecked(levelSettingsMenuCritical, PrettyLog.LogLevel.Critical == PrettyLog.prettyLogSettings.levelToLog);
		}

		private static void UpdateLevelToLog(PrettyLog.LogLevel logLevel)
		{
			PrettyLog.prettyLogSettings.levelToLog = logLevel;
			UpdateCheckStatusLogLevel(PrettyLog.prettyLogSettings.levelToLog);
		}

		private static void ToogleLogCategory(PrettyLog.LogCategory logCategory, string menuName)
		{
			PrettyLog.prettyLogSettings.logCategorySettings.ToogleLogCategory(logCategory);
			Menu.SetChecked(menuName, PrettyLog.prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(logCategory));
		}
		#endregion

		#region Debug
		//[MenuItem(menuName + "/Show All Logs", false, 1000)]
		private static void TestLogs()
		{
			// Script
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script, "Test debug Script !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script, "Test Info Script !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script, "Test TestResult Script !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script, "Test Warning Script !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script, "Test Error Script !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script, "Test Critical Script !");

			// Script_Init
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script_Init, "Test debug Script_Init !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Init, "Test Info Script_Init !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script_Init, "Test TestResult Script_Init !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script_Init, "Test Warning Script_Init !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script_Init, "Test Error Script_Init !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script_Init, "Test Critical Script_Init !");

			// Script_Data
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script_Data, "Test debug Script_Data !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Data, "Test Info Script_Data !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script_Data, "Test TestResult Script_Data !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script_Data, "Test Warning Script_Data !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script_Data, "Test Error Script_Data !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script_Data, "Test Critical Script_Data !");

			// Script_Utils
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script_Utils, "Test debug Script_Utils !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Utils, "Test Info Script_Utils !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script_Utils, "Test TestResult Script_Utils !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script_Utils, "Test Warning Script_Utils !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script_Utils, "Test Error Script_Utils !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script_Utils, "Test Critical Script_Utils !");

			// Script_Engine
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script_Engine, "Test debug Script_Engine !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Engine, "Test Info Script_Engine !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script_Engine, "Test TestResult Script_Engine !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script_Engine, "Test Warning Script_Engine !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script_Engine, "Test Error Script_Engine !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script_Engine, "Test Critical Script_Engine !");

			// Script_Editor
			PrettyLog.LogDebug(PrettyLog.LogCategory.Script_Editor, "Test debug Script_Editor !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Script_Editor, "Test Info Script_Editor !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Script_Editor, "Test TestResult Script_Editor !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Script_Editor, "Test Warning Script_Editor !");
			PrettyLog.LogError(PrettyLog.LogCategory.Script_Editor, "Test Error Script_Editor !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Script_Editor, "Test Critical Script_Editor !");

			// UI
			PrettyLog.LogDebug(PrettyLog.LogCategory.UI, "Test debug UI !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.UI, "Test Info UI !");
			PrettyLog.LogTest(PrettyLog.LogCategory.UI, "Test TestResult UI !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.UI, "Test Warning UI !");
			PrettyLog.LogError(PrettyLog.LogCategory.UI, "Test Error UI !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.UI, "Test Critical UI !");

			// Network
			PrettyLog.LogDebug(PrettyLog.LogCategory.Network, "Test debug network !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Network, "Test Info network !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Network, "Test TestResult network !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Network, "Test Warning network !");
			PrettyLog.LogError(PrettyLog.LogCategory.Network, "Test Error network !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Network, "Test Critical network !");

			// Game
			PrettyLog.LogDebug(PrettyLog.LogCategory.Game, "Test debug Game !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Game, "Test Info Game !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Game, "Test TestResult Game !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Game, "Test Warning Game !");
			PrettyLog.LogError(PrettyLog.LogCategory.Game, "Test Error Game !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Game, "Test Critical Game !");

			// Game_Loop
			PrettyLog.LogDebug(PrettyLog.LogCategory.Game_Loop, "Test debug Game_Loop !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Game_Loop, "Test Info Game_Loop !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Game_Loop, "Test TestResult Game_Loop !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Game_Loop, "Test Warning Game_Loop !");
			PrettyLog.LogError(PrettyLog.LogCategory.Game_Loop, "Test Error Game_Loop !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Game_Loop, "Test Critical Game_Loop !");

			// Gamee_Save
			PrettyLog.LogDebug(PrettyLog.LogCategory.Game_Save, "Test debug Game_Save !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Game_Save, "Test Info Game_Save !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Game_Save, "Test TestResult Game_Save !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Game_Save, "Test Warning Game_Save !");
			PrettyLog.LogError(PrettyLog.LogCategory.Game_Save, "Test Error Game_Save !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Game_Save, "Test Critical Game_Save !");

			// Game_Load
			PrettyLog.LogDebug(PrettyLog.LogCategory.Game_Load, "Test debug Game_Load !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Game_Load, "Test Info Game_Load !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Game_Load, "Test TestResult Game_Load !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Game_Load, "Test Warning Game_Load !");
			PrettyLog.LogError(PrettyLog.LogCategory.Game_Load, "Test Error Game_Load !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Game_Load, "Test Critical Game_Load !");

			// Data
			PrettyLog.LogDebug(PrettyLog.LogCategory.Data, "Test debug Data !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Data, "Test Info Data !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Data, "Test TestResult Data !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Data, "Test Warning Data !");
			PrettyLog.LogError(PrettyLog.LogCategory.Data, "Test Error Data !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Data, "Test Critical Data !");

			// Data_Save
			PrettyLog.LogDebug(PrettyLog.LogCategory.Data_Save, "Test debug Data_Save !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Data_Save, "Test Info Data_Save !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Data_Save, "Test TestResult Data_Save !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Data_Save, "Test Warning Data_Save !");
			PrettyLog.LogError(PrettyLog.LogCategory.Data_Save, "Test Error Data_Save !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Data_Save, "Test Critical Data_Save !");

			// Data_Load
			PrettyLog.LogDebug(PrettyLog.LogCategory.Data_Load, "Test debug Data_Load !");
			PrettyLog.LogInfo(PrettyLog.LogCategory.Data_Load, "Test Info Data_Load !");
			PrettyLog.LogTest(PrettyLog.LogCategory.Data_Load, "Test TestResult Data_Load !");
			PrettyLog.LogWarning(PrettyLog.LogCategory.Data_Load, "Test Warning Data_Load !");
			PrettyLog.LogError(PrettyLog.LogCategory.Data_Load, "Test Error Data_Load !");
			PrettyLog.LogCritical(PrettyLog.LogCategory.Data_Load, "Test Critical Data_Load !");
		}
		#endregion
	}
}
