﻿using UnityEngine;
using System;
using System.Runtime.CompilerServices;

namespace UnityPrettyLog
{
	public class PrettyLog
	{

		#region Events
		/// <summary>
		/// called each new pretty log, string 1 is the identifier string 2 the message
		/// </summary>
		public static event Action<LogLevel, LogCategory, string, string> OnNewPrettyLog;
		#endregion

		#region Enums
		public enum LogLevel
		{
			Debug = 50,
			Info = 100,
			Test = 150,
			Warning = 200,
			Error = 300,
			Critical = 400
		}

		public enum LogCategory
		{
			Script = 100,
			Script_Init = 105,
			Script_Data = 110,
			Script_Utils = 115,
			Script_Engine = 120,
			Script_Editor = 125,
			UI = 200,
			Network = 300,
			Game = 400,
			Game_Loop = 410,
			Game_Save = 420,
			Game_Load = 425,
			Data = 500,
			Data_Save = 510,
			Data_Load = 520
		}
		#endregion

		#region Instances
		private const string prettyLogSettingsPath = "PrettyLogSettings";
		public static readonly PrettyLogSettings prettyLogSettings ;
		#endregion

		#region Getter / Setter
		private static LogType LogLevelToUnityLogLevel(LogLevel logLevel)
		{
			switch (logLevel)
			{
				case LogLevel.Info:
				case LogLevel.Debug:
					return LogType.Log;
				case LogLevel.Warning:
					return LogType.Warning;
				case LogLevel.Error:
				case LogLevel.Critical:
					return LogType.Error;

				default:
					return LogType.Log;
			}
		}
		#endregion

		#region Constructor
		static PrettyLog()
		{
			prettyLogSettings = Resources.Load<PrettyLogSettings>(prettyLogSettingsPath);
			LogInfo(PrettyLog.LogCategory.Script_Init, "Unity pretty log settings loaded");
		}
		#endregion

		#region Log Printing
		/// <summary>
		/// Print a log in the unity console and call <see cref="OnNewPrettyLog"/>
		/// </summary>
		/// <param name="logCategory">A category used for log filter</param>
		/// <param name="message">Message to log</param>
		/// <param name="context">Object to which the message applies.</param>
		private static void Log(LogLevel logLevel, LogCategory logCategory, string message, UnityEngine.Object context, string callerName, string callerPath, int callerLine)
		{
			string[] splitPath = callerPath.Split('\\');
			string className = splitPath[splitPath.Length - 1];

			OnNewPrettyLog?.Invoke(logLevel, logCategory, $"({logLevel} : {logCategory}) [{className} : {callerName} : {callerLine}]", message);

			if (logLevel < prettyLogSettings.levelToLog || !prettyLogSettings.logCategorySettings.IsLogCategoryAllowed(logCategory))
				return;

			Color color = prettyLogSettings.logLevelColor.GetLogLevelColor(logLevel);
			LogType logType = LogLevelToUnityLogLevel(logLevel);

			string logMessage = $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>({logLevel} : {logCategory}) [{className} : {callerName} : {callerLine}]</color> {message}";

			switch (logType)
			{
				case LogType.Log:
					Debug.Log(logMessage, context);
					break;
				case LogType.Warning:
					Debug.LogWarning(logMessage, context);
					break;
				case LogType.Error:
					Debug.LogError(logMessage, context);
					break;

				default:
					Debug.Log(logMessage, context);
					break;
			}
		}
		/// <summary>
		/// Create a log with a debug level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogDebug(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Debug, logCategory, message, context, callerName, callerPath, callerLine);

		/// <summary>
		/// Create a log with a info level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogInfo(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Info, logCategory, message, context, callerName, callerPath, callerLine);

		/// <summary>
		/// Create a log with a Test level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogTest(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Test, logCategory, message, context, callerName, callerPath, callerLine);

		/// <summary>
		/// Create a log with a warning level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogWarning(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Warning, logCategory, message, context, callerName, callerPath, callerLine);

		/// <summary>
		/// Create a log with a error level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogError(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Error, logCategory, message, context, callerName, callerPath, callerLine);

		/// <summary>
		/// Create a log with a critical level <br/>
		/// The Caller member are automatically filled by the compiler they are not need to be filled ! <br/>
		/// <inheritdoc cref="Log"/>
		/// <param name="callerName">Determine by the compiler</param>
		/// <param name="callerPath">Determine by the compiler</param>
		/// <param name="callerLine">Determine by the compiler</param>
		/// </summary>
		public static void LogCritical(LogCategory logCategory, string message, UnityEngine.Object context = null, [CallerMemberName] string callerName = "", [CallerFilePath] string callerPath = "", [CallerLineNumberAttribute] int callerLine = 0) => Log(LogLevel.Critical, logCategory, message, context, callerName, callerPath, callerLine);
		#endregion
	}
}
