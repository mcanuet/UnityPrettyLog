﻿using System;
using UnityEngine;

namespace UnityPrettyLog
{
	//[CreateAssetMenu(fileName = "PrettyLogSettings", menuName = "ScriptableObjects/PrettyLogSettings", order = 1)]
	public class PrettyLogSettings : ScriptableObject
	{
		#region Instances
		[Header("Level Settings")]
		public LogLevelColor logLevelColor = new LogLevelColor();
		public LogLevelSprite logLevelSprite = new LogLevelSprite();
		[Header("Display Settings")]
		public PrettyLog.LogLevel levelToLog = PrettyLog.LogLevel.Debug;
		public LogCategorySettings logCategorySettings = new LogCategorySettings();
		[Header("Prefabs")]
		public GameObject PrettyLogWindow;
		#endregion

		#region Class Definition
		[Serializable]
		public class LogLevelColor
		{
			public Color Debug = Color.white;
			public Color Info = Color.cyan;
			public Color Test = Color.green;
			public Color Warning = Color.yellow;
			public Color Error = Color.red;
			public Color Critical = Color.red;

			public Color GetLogLevelColor(PrettyLog.LogLevel logLevel)
			{
				switch (logLevel)
				{
					case PrettyLog.LogLevel.Debug:
						return Debug;
					case PrettyLog.LogLevel.Info:
						return Info;
					case PrettyLog.LogLevel.Test:
						return Test;
					case PrettyLog.LogLevel.Warning:
						return Warning;
					case PrettyLog.LogLevel.Error:
						return Error;
					case PrettyLog.LogLevel.Critical:
						return Critical;
					default:
						return Color.white;
				}
			}
		}

		[Serializable]
		public class LogLevelSprite
		{
			public Sprite Debug;
			public Sprite Info;
			public Sprite Test;
			public Sprite Warning;
			public Sprite Error;
			public Sprite Critical;

			public Sprite GetLogLevelSprite(PrettyLog.LogLevel logLevel)
			{
				switch (logLevel)
				{
					case PrettyLog.LogLevel.Debug:
						return Debug;
					case PrettyLog.LogLevel.Info:
						return Info;
					case PrettyLog.LogLevel.Test:
						return Test;
					case PrettyLog.LogLevel.Warning:
						return Warning;
					case PrettyLog.LogLevel.Error:
						return Error;
					case PrettyLog.LogLevel.Critical:
						return Critical;
					default:
						return null;
				}
			}
		}

		[Serializable]
		public class LogCategorySettings
		{
			public bool Script = true;
			public bool Script_Init = true;
			public bool Script_Data = true;
			public bool Script_Utils = true;
			public bool Script_Engine = true;
			public bool Script_Editor = true;
			public bool UI = true;
			public bool Network = true;
			public bool Game = true;
			public bool Game_Loop = true;
			public bool Game_Save = true;
			public bool Game_Load = true;
			public bool Data = true;
			public bool Data_Save = true;
			public bool Data_Load = true;

			#region Getter / Setter
			public bool IsLogCategoryAllowed(PrettyLog.LogCategory logCategory)
		{
			switch(logCategory)
			{
				case PrettyLog.LogCategory.Script:
					return Script;
				case PrettyLog.LogCategory.Script_Init:
					return Script_Init;
				case PrettyLog.LogCategory.Script_Data:
					return Script_Data;
				case PrettyLog.LogCategory.Script_Utils:
					return Script_Utils;
				case PrettyLog.LogCategory.Script_Engine:
					return Script_Engine;
				case PrettyLog.LogCategory.Script_Editor:
					return Script_Editor;
				case PrettyLog.LogCategory.UI:
					return UI;
				case PrettyLog.LogCategory.Network:
					return Network;
				case PrettyLog.LogCategory.Game:
					return Game;
				case PrettyLog.LogCategory.Game_Loop:
					return Game_Loop;
				case PrettyLog.LogCategory.Game_Save:
					return Game_Save;
				case PrettyLog.LogCategory.Game_Load:
					return Game_Load;
				case PrettyLog.LogCategory.Data:
					return Data;
				case PrettyLog.LogCategory.Data_Save:
					return Data_Save;
				case PrettyLog.LogCategory.Data_Load:
					return Data_Load;
				default:
					return true;
			}
		}

			public void ToogleLogCategory(PrettyLog.LogCategory logCategory)
			{
				switch(logCategory)
				{
					case PrettyLog.LogCategory.Script:
						Script = !Script;
						break;
					case PrettyLog.LogCategory.Script_Init:
						Script_Init = !Script_Init;
						break;
					case PrettyLog.LogCategory.Script_Data:
						Script_Data = !Script_Data;
						break;
					case PrettyLog.LogCategory.Script_Utils:
						Script_Utils = !Script_Utils;
						break;
					case PrettyLog.LogCategory.Script_Engine:
						Script_Engine =!Script_Engine;
						break;
					case PrettyLog.LogCategory.Script_Editor:
						Script_Editor = !Script_Editor;
						break;
					case PrettyLog.LogCategory.UI:
						UI= !UI;
						break;
					case PrettyLog.LogCategory.Network:
						Network = !Network;
						break;
					case PrettyLog.LogCategory.Game:
						Game = !Game;
						break;
					case PrettyLog.LogCategory.Game_Loop:
						Game_Loop = !Game_Loop;
						break;
					case PrettyLog.LogCategory.Game_Save:
						Game_Save = !Game_Save;
						break;
					case PrettyLog.LogCategory.Game_Load:
						Game_Load = !Game_Load;
						break;
					case PrettyLog.LogCategory.Data:
						Data = !Data;
						break;
					case PrettyLog.LogCategory.Data_Save:
						Data_Save = !Data_Save;
						break;
					case PrettyLog.LogCategory.Data_Load:
						Data_Load = !Data_Load;
						break;
				}
			}
			#endregion
		}
		#endregion
	}
}
