using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityPrettyLog
{
	public class PrettyLog_UI : MonoBehaviour
	{
		#region Instances
		[Header("Settings")]
		[SerializeField] private KeyCode openWindowLogKey;
		[SerializeField] private GameObject logLinePrefab;
		[SerializeField] private bool autoShowLog;
		[SerializeField] private PrettyLog.LogLevel levelToAutoShowLog;
		[Header("UI")]
		[SerializeField] private RectTransform root;
		[SerializeField] private Transform logLinesParent;
		[SerializeField] private GameObject categoriesFrame;

		private static PrettyLog_UI instance;
		public static readonly Dictionary<PrettyLog.LogLevel, bool> logLevelVisibility = new Dictionary<PrettyLog.LogLevel, bool>();
		public static readonly Dictionary<PrettyLog.LogCategory, bool> logCategoryVisibility = new Dictionary<PrettyLog.LogCategory, bool>();

		private float timeOfTravel = 0.5f;
		private float currentTime = 0f;
		private float normalized;
		#endregion

		#region events
		public static Action<PrettyLog.LogLevel?, PrettyLog.LogCategory?> OnToogleLog;
		#endregion

		#region flow
		PrettyLog_UI()
		{
			if (instance != this)
				instance = this;

			logLevelVisibility.Clear();
			logLevelVisibility.Add(PrettyLog.LogLevel.Debug, true);
			logLevelVisibility.Add(PrettyLog.LogLevel.Info, true);
			logLevelVisibility.Add(PrettyLog.LogLevel.Test, true);
			logLevelVisibility.Add(PrettyLog.LogLevel.Warning, true);
			logLevelVisibility.Add(PrettyLog.LogLevel.Error, true);
			logLevelVisibility.Add(PrettyLog.LogLevel.Critical, true);

			logCategoryVisibility.Clear();
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script_Init, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script_Data, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script_Utils, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script_Engine, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Script_Editor, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.UI, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Network, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Game, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Game_Loop, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Game_Load, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Game_Save, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Data, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Data_Load, true);
			logCategoryVisibility.Add(PrettyLog.LogCategory.Data_Save, true);
		}

		private void Awake()
		{
			PrettyLog.OnNewPrettyLog += OnNewPrettyLog;
		}

		private void OnDestroy()
		{
			PrettyLog.OnNewPrettyLog -= OnNewPrettyLog;
		}

		private void Update()
		{
			if (Input.GetKeyUp(openWindowLogKey))
				ShowWindowLog(root.anchoredPosition.y > 0f);
		}

		private void ShowWindowLog(bool show)
		{
			Vector3 calculatedPos = root.anchoredPosition;
			float y = show ? 0f : root.rect.height * 1;
			calculatedPos.y = y;
			currentTime = 0f;
			IEnumerator enumerator = MoveMenu(root.anchoredPosition, calculatedPos);
			StartCoroutine(enumerator);
		}
		#endregion

		#region event handler
		private void OnNewPrettyLog(PrettyLog.LogLevel logLevel, PrettyLog.LogCategory logCategory, string identifire, string message)
		{
			GameObject logLine_GO = Instantiate(logLinePrefab, logLinesParent).gameObject;
			LogLine_UI logLine_UI = logLine_GO.GetComponent<LogLine_UI>();
			bool isLogVisible = logLevelVisibility[logLevel] && logCategoryVisibility[logCategory];
			logLine_UI.Init(logLevel, logCategory, identifire, message, isVisible: isLogVisible);
			if (autoShowLog && logLevel >= levelToAutoShowLog)
				ShowWindowLog(true);
		}
		#endregion

		#region button flow
		public static void ToogleLogLevel(PrettyLog.LogLevel logLevel)
		{
			logLevelVisibility[logLevel] = !logLevelVisibility[logLevel];
			OnToogleLog?.Invoke(logLevel, null);
		}

		public static void ToogleLogCategory(PrettyLog.LogCategory logCategory)
		{
			logCategoryVisibility[logCategory] = !logCategoryVisibility[logCategory];
			OnToogleLog?.Invoke(null, logCategory);
		}

		public void B_ClearLogs()
		{
			for (int childIndex = 0; childIndex < logLinesParent.childCount; ++childIndex)
				Destroy(logLinesParent.GetChild(childIndex).gameObject);
		}

		public void B_ToogleCategoriesFrame() => categoriesFrame.SetActive(!categoriesFrame.activeInHierarchy);

		public void B_TooglLogWindow() => ShowWindowLog(root.anchoredPosition.y > 0f);
		#endregion

		#region Coroutine
		private IEnumerator MoveMenu(Vector3 startPos, Vector3 endPos)
		{

			while (currentTime <= timeOfTravel)
			{
				currentTime += Time.deltaTime;
				normalized = currentTime / timeOfTravel;

				root.anchoredPosition = Vector3.Lerp(startPos, endPos, normalized);
				yield return null;
			}
		}
		#endregion
	}
}
