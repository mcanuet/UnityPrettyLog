using UnityEngine;
using UnityEngine.UI;

namespace UnityPrettyLog
{
	public class LogLine_UI : MonoBehaviour
	{
		#region Instances
		[Header("UI")]
		[SerializeField] private GameObject root;
		[SerializeField] private Image backgroundImage;
		[SerializeField] private Color oddColor;
		[SerializeField] private Color evenColor;
		[Header("Log element")]
		[SerializeField] private Image levelImage;
		[SerializeField] private Text identifierText;
		[SerializeField] private Text messageText;

		private PrettyLog.LogLevel logLevel;
		private PrettyLog.LogCategory logCategory;
		#endregion

		#region Flow
		private void OnDestroy()
		{
			PrettyLog_UI.OnToogleLog -= ToogleLog;
		}

		public void Init(PrettyLog.LogLevel level, PrettyLog.LogCategory category, string identifier, string message, bool isOdd = false, bool isVisible = true)
		{
			logLevel = level;
			logCategory = category;
			backgroundImage.color = isOdd ? oddColor : evenColor;
			levelImage.sprite = PrettyLog.prettyLogSettings.logLevelSprite.GetLogLevelSprite(level);
			identifierText.text = identifier;
			identifierText.color = PrettyLog.prettyLogSettings.logLevelColor.GetLogLevelColor(level);
			messageText.text = message;
			root.SetActive(isVisible);
			PrettyLog_UI.OnToogleLog += ToogleLog;
		}

		private void ToogleLog(PrettyLog.LogLevel? logLevel, PrettyLog.LogCategory? logCategory)
		{
			if (logLevel == this.logLevel || logCategory == this.logCategory)
				root.SetActive(PrettyLog_UI.logLevelVisibility[this.logLevel] && PrettyLog_UI.logCategoryVisibility[this.logCategory]);
		}
		#endregion
	}
}

