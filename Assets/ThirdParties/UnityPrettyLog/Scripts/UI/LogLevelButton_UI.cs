using UnityEngine;
using UnityEngine.UI;

namespace UnityPrettyLog
{
    public class LogLevelButton_UI : MonoBehaviour
	{
		#region Intances
		[SerializeField] private PrettyLog.LogLevel logLevel;
		[SerializeField] private Image background;
		[SerializeField] private Color normalColor;
		[SerializeField] private Color clickedColor;

		private bool clicked = false;
		#endregion

		#region Flow
		public void B_Clicked()
		{
			clicked = !clicked;
			background.color = clicked ? clickedColor : normalColor;
			PrettyLog_UI.ToogleLogLevel(logLevel);
		}
		#endregion
	}
}
