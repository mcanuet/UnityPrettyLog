using UnityEngine;
using UnityEngine.UI;

namespace UnityPrettyLog
{
	public class CategoryButton_UI : MonoBehaviour
	{
		[SerializeField] private GameObject tickImage;
		[SerializeField] private PrettyLog.LogCategory logCategory;
		[SerializeField] private Text logText;

		private void Awake()
		{
			logText.text = logCategory.ToString();
		}

		public void B_OnButtonCategoryClick()
		{
			tickImage.SetActive(!tickImage.activeInHierarchy);
			PrettyLog_UI.ToogleLogCategory(logCategory);
		}
	}
}
