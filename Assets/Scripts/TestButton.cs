using UnityEngine;
using UnityPrettyLog;

public class TestButton : MonoBehaviour
{
	public void B_TestLog()
	{
		PrettyLog.LogDebug(PrettyLog.LogCategory.Script, "Test debug Script !");
		PrettyLog.LogInfo(PrettyLog.LogCategory.Script, "Test Info Script !");
		PrettyLog.LogTest(PrettyLog.LogCategory.Script, "Test TestResult Script !");
		PrettyLog.LogWarning(PrettyLog.LogCategory.Script, "Test Warning Script !");
		PrettyLog.LogError(PrettyLog.LogCategory.Script, "Test Error Script !");
		PrettyLog.LogCritical(PrettyLog.LogCategory.Script, "Test Critical Script !");
	}
}
